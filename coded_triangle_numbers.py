import time
start_time = time.time()

def triangle_number_in_range(limit):
    triangle_numbers = []
    for i in range(1,limit):
        triangle_numbers.append(int(0.5*i*(i+1)))
    return triangle_numbers

def count_words_triangle(limit):
    words = sorted(open('p042_words.txt').read().replace('"', '').split(","))
    triangle_numbers = triangle_number_in_range(limit)
    number_triangle_words = 0
    for word in words:
        sum_word = 0
        if sum([ord(i)-64 for i in word]) in triangle_numbers:
            number_triangle_words += 1
    return number_triangle_words

print(count_words_triangle(30))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )